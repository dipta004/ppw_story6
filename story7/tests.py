from django.test import TestCase

# Create your tests here.
from django.urls import resolve, reverse

from story7.views import index


class Story7Test(TestCase):
    def test_url_exist(self):
        c = self.client;
        response = c.get(reverse('story7:landing_page'))
        self.assertEqual(response.status_code,200)

    def test_use_index_function(self):
        response = resolve(reverse('story7:landing_page'))
        self.assertEqual(response.func,index)

    def test_using_same_template(self):
        response = self.client.get(reverse('story7:landing_page'))
        self.assertTemplateUsed(response,'story7/story7.html')