from django.contrib.auth.models import User
from django.test import TestCase, Client
from . import views
import json

# Create your tests here.
from django.urls import resolve


class Story8Test(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="fadhil",password="12345")
        user.save()

    def test_url_page(self):
        response = self.client.post('/story9/login/', data={
            'username': "fadhil",
            'password': '12345'
        })

        response = self.client.get("/story8/")
        self.assertEqual(response.status_code,200)

    def test_url_AJAX(self):
        response = self.client.get("/story8/all")
        self.assertEqual(response.status_code,200)

    def test_page_using_function(self):
        response = resolve("/story8/")
        self.assertEqual(response.func,views.index)

    def test_page_using_AJAX(self):
        response = resolve("/story8/all")
        self.assertEqual(response.func,views.google_books)

    def test_using_template(self):
        response = self.client.post('/story9/login/',data={
            'username':"fadhil",
            'password':'12345'
        })

        response = self.client.get("/story8/")
        self.assertTemplateUsed(response,"story8/index.html")

    def test_response_JSON_AJAX(self):
        response = self.client.get("/story8/all")
        self.assertEqual(type(json.loads(response.content)),dict)