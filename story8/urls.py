from django.urls import path

from story8.views import google_books, index

urlpatterns = [
    path("",index),
    path('<str:keyword>',google_books),
]