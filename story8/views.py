import requests
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.
def google_books(request,keyword):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q='+keyword)
    return JsonResponse(response.json())
    # return HttpResponse()

@login_required(login_url='/story9/login/')
def index(request):
    #return HttpResponse()
    return render(request,'story8/index.html',context={'name':request.user.username})