build-load:
	bash -c "docker-compose -f docker-compose.yml up -d --b"

run-load:
	bash -c "docker-compose up"

remove-load:
	bash -c "docker-compose down"