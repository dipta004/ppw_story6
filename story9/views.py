from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.
from story9.forms import UserLoginForm, UserCreateForm


def login_view(request):
    if request.user.is_authenticated:
        return redirect('/story8/')
    else:
        form = UserLoginForm()
        if request.method == "POST":
            form = UserLoginForm(request=request,data=request.POST)
            if form.is_valid():
                user = form.get_user()
                login(request,user=user)
                if 'next' in request.POST:
                    return redirect(request.POST.get('next'))
                return redirect('/story8/')
            else:
                return render(request, 'story9/signin.html', context={'form': form,'error':form.get_invalid_login_error()})
        return render(request, 'story9/signin.html', context={'form':form})

def logout_view(request):
    if request.method == "POST":
        logout(request)
        return redirect('/story9/login/')
    return HttpResponse()

def signup(request):
    if request.user.is_authenticated:
        return redirect('/story8/')
    else:
        form = UserCreateForm()
        if request.method == "POST":
            form = UserCreateForm(data=request.POST)
            if form.is_valid():
                user = form.save()
                login(request,user=user)
                if 'next' in request.POST:
                    return redirect(request.POST.get('next'))
                return redirect('/story8/')
        return render(request,'story9/signup.html',context={'form':form})