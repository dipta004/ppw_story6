import os
from time import sleep

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, LiveServerTestCase
# Create your tests here.
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from PPW_Story6.settings import BASE_DIR
from story9.views import login_view, logout_view, signup


class Story9ViewTest(TestCase):
    def setUp(self):
        user = User.objects.create_user(username="fadhil",password="12345")
        user.save()

    def test_using_login_views(self):
        response = resolve('/story9/login/')
        self.assertEqual(response.func,login_view)

    def test_using_logout_view(self):
        response = resolve('/story9/logout/')
        self.assertEqual(response.func,logout_view)

    def test_using_signup_view(self):
        response = resolve('/story9/signup/')
        self.assertEqual(response.func,signup)

    def test_signup(self):
        response = self.client.post('/story9/signup/',data={
            'username':"boom",
            'password1':'yeeha123',
            'password2': 'yeeha123',
        })
        user = User.objects.all()
        self.assertEqual(len(user),2)
        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code,302)
        self.assertRedirects(expected_url='/story8/',response=response)

    def test_signup_fail(self):
        response = self.client.post('/story9/signup/',data={
            'username':"boom",
            'password1':'12345667',
            'password2': '1234566',
        })
        user = get_user_model().objects.all()

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(len(user),1)
        self.assertEqual(response.status_code,200)

    def test_signin(self):
        response = self.client.post('/story9/login/',data={
            'username':"fadhil",
            'password':'12345'
        })

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story8/',response=response)

    def test_already_signin(self):
        response = self.client.post('/story9/login/',data={
            'username':"fadhil",
            'password':'12345'
        })

        response = self.client.get('/story9/login/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story8/',response=response)

        response = self.client.get('/story9/signup/')

        self.assertIn('_auth_user_id',self.client.session)
        self.assertIn('_auth_user_backend', self.client.session)
        self.assertIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story8/',response=response)


    def test_signin_fail(self):
        response = self.client.post('/story9/login/',data={
            'username':'something',
            'password':'123123123'
        })

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 200)

    def test_signout(self):
        response = self.client.post('/story9/login/',data={
            'username':"fadhil",
            'password':'12345'
        })

        response = self.client.post('/story9/logout/')

        self.assertNotIn('_auth_user_id',self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(expected_url='/story9/login/',response=response)

    def test_using_signin_template(self):
        response = self.client.get('/story9/login/')

        self.assertTemplateUsed(response,'story9/signin.html')

    def test_using_signup_template(self):
        response = self.client.get('/story9/signup/')

        self.assertTemplateUsed(response,'story9/signup.html')

class LiveTestCase(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        user = User.objects.create_user(username="fadhil",password="12345")
        user.save()
        super(LiveTestCase,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(LiveTestCase,self).tearDown()

    def test_signin_required(self):
        browser = self.browser
        browser.get(self.live_server_url+"/story8/")

        next = browser.find_element_by_name('next')
        self.assertEqual('Sign In', browser.title)
        self.assertEqual('/story8/',next.get_property('value'))
        self.assertEqual(self.live_server_url+"/story9/login/?next=/story8/",browser.current_url)

        form_username = browser.find_element_by_id('id_username')
        form_password = browser.find_element_by_id('id_password')
        submit_btn = browser.find_element_by_id('login-btn')

        form_username.send_keys('fadhil')
        form_password.send_keys('12345')

        submit_btn.click()

        self.assertEqual('Story8',browser.title)

    def test_signup_redirect(self):
        browser = self.browser
        browser.get(self.live_server_url+"/story8/")

        create_btn = browser.find_element_by_id('create-btn')
        create_btn.click()

        next = browser.find_element_by_name('next')
        self.assertEqual('Sign Up', browser.title)
        self.assertEqual('/story8/',next.get_property('value'))
        self.assertEqual(self.live_server_url+"/story9/signup/?next=/story8/",browser.current_url)

        form_username = browser.find_element_by_id('id_username')
        form_password = browser.find_element_by_id('id_password1')
        form_password1 = browser.find_element_by_id('id_password2')
        submit_btn = browser.find_element_by_id('create-submit-btn')

        form_username.send_keys('dipta004')
        form_password.send_keys('dskhjsdkgjsdjg1346346')
        form_password1.send_keys('dskhjsdkgjsdjg1346346')
        submit_btn.click()

        self.assertEqual('Story8',browser.title)

    def test_signout(self):
        browser = self.browser
        browser.get(self.live_server_url+"/story8/")

        form_username = browser.find_element_by_id('id_username')
        form_password = browser.find_element_by_id('id_password')
        submit_btn = browser.find_element_by_id('login-btn')

        form_username.send_keys('fadhil')
        form_password.send_keys('12345')

        submit_btn.click()

        btn_logout = browser.find_element_by_id('logout-btn')
        btn_logout.click()

        self.assertEqual('Sign In',browser.title)
        self.assertNotIn('_auth_user_id', self.client.session)
        self.assertNotIn('_auth_user_backend', self.client.session)
        self.assertNotIn('_auth_user_hash', self.client.session)