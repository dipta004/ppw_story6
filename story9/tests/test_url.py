from django.test import TestCase

class Story9UrlsTest(TestCase):
    def test_url_login_exist(self):
        response = self.client.get('/story9/login/')
        self.assertEqual(response.status_code,200)

    def test_url_logout_exist(self):
        response = self.client.get('/story9/logout/')
        self.assertEqual(response.status_code,200)

    def test_url_signup_exist(self):
        response = self.client.get('/story9/signup/')
        self.assertEqual(response.status_code,200)