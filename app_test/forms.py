from django import forms

from app_test.models import Status


class StatusForm(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']