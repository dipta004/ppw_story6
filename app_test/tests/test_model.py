from django.test import TestCase

from app_test.models import Status


class ModelTest(TestCase):
    def test_str(self):
        obj = Status(status="a"*100)
        obj.save()

        self.assertEqual(str(obj),"a"*50)