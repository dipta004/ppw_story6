from django.test import TestCase, Client
from django.urls import resolve, reverse

from app_test.models import Status
from app_test.views import index


class ViewsTest(TestCase):
    def setUp(self):
        self.landing_url=reverse("landing_page")

    def test_using_index_function(self):
        response = resolve(self.landing_url)

        self.assertEqual(response.func,index)

    def test_using_same_tempalte(self):
        response = self.client.get(self.landing_url)
        self.assertTemplateUsed(response,'app_test/landing.html')

    def test_form_element_complete(self):
        response = self.client.get(self.landing_url)
        html = response.content.decode('utf-8')
        self.assertIn("id=\"id_status\"",html) #input "status" exist
        self.assertIn("type=\"submit\"",html) #button submit exist

    def test_not_valid_form(self):
        data = {
            'status': "a"*301 #character ammount greater than 300
        }
        response = self.client.post(self.landing_url,data=data)

        status_ammount = Status.objects.all().count()

        self.assertEqual(status_ammount, 0)
        self.assertEqual(response.status_code,200)

    def test_add_status(self):
        data = {
            'status':"SdANTUY"
        }
        response = self.client.post(self.landing_url,data=data)
        first_status = Status.objects.get(id=1)
        status_ammount = Status.objects.all().count()

        self.assertEqual(status_ammount,1)
        self.assertEqual(first_status.status,data['status'])
        self.assertRedirects(response,self.landing_url)