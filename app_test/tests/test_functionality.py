import datetime
import os

from django.test import LiveServerTestCase, TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from PPW_Story6.settings import BASE_DIR

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,"chromedriver"),chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest,self).tearDown()

    def test_post_status(self):
        browser = self.browser
        browser.get(self.live_server_url+"/")

        form_status = browser.find_element_by_id("id_status")
        submit_btn = browser.find_element_by_id("btn-submit")

        form_status.send_keys("Coba Coba")
        submit_btn.send_keys(Keys.RETURN)

        self.assertIn("Coba Coba",browser.page_source)
        self.assertIn(str(datetime.datetime.now().strftime("%b. %d, %Y, %I:%M").replace(" 0"," ")),browser.page_source)

    def test_title_page(self):
        browser = self.browser
        browser.get(self.live_server_url+"/")

        self.assertEqual(browser.title, "Landing Page")