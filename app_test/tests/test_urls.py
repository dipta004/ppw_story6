from django.test import TestCase, Client
from django.urls import reverse


class UrlsTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_url_landing_page_exist(self):
        response = self.client.get(reverse("landing_page"))
        self.assertEqual(response.status_code,200)

    def test_url_not_exists(self):
        response = self.client.get("/unknown/")
        self.assertEqual(response.status_code,404)