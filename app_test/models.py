import datetime

from django.db import models

# Create your models here.
class Status(models.Model):
    status = models.TextField(max_length=300)
    date_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status[:50]