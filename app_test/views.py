from django.shortcuts import render, redirect
from django.http import HttpResponse

# Create your views here.
from django.urls import reverse

from app_test.forms import StatusForm
from app_test.models import Status


def index(request):
    form = StatusForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            obj = Status(status=form.cleaned_data['status'])
            obj.save()
            return redirect("landing_page")
    return render(request,'app_test/landing.html',{'forms':StatusForm(),'status':Status.objects.all(),'server':request.META['REMOTE_ADDR'],'port':request.META['SERVER_PORT']})
